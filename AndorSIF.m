classdef AndorSIF < handle % we must inherit from handle class to update properties with new values/data while methods are run in instance.
    % Class for importing Andor Solis SIF files and converting these to a more useable format. This class is an adaptation to an Object Oriented approach for the already existing MATLab code available fo this purpose, that implements the Andor SIF API.
    %
    % Dependencies:
    %   +   atsifio;    must be in MATLab path
    %   +   viridis.m   Colourmap of choise that outclasses old default colourmaps like 'jet' and 'parula' in terms of perceptual brightness, perception by colourblind people (deuteranomaly) and grey scale printing. It is nowadays the default map in SciPy. Source: https://bids.github.io/colormap/
    %
    %
    % Properties:
    %   +   succes        parameter for when SIF has been fully read succesfully. Default = 0
    %   +   M             The main structure where the information of all the frames of a *.sif files get saved in. Should be empty at initialization.
    %   +   calibration   Array containing the wavelength calibration values for each pixel index in X direction, as according to Andor software.
    %   +   Reference     The frame number for AndorSIF.FrameID() for which both LED and Plasma are turned on, so we correctly can determine the absorption.
    %   +   frame_offset  Integer, default=0. Used to introduce an offset in the 4 frames we view to determine our starting position in the trigger scheme, in case the fist four frames are inconclusive. Set as a property because we want to be able to retrieve this value from an instance in case it is needed, to discard useless frames.
    %   +   ROI           Array contraining vertices/dimensions of Regions of Interest that have been identified by obj.DefineROI(), or can be set to use predefined ROI's in multiple instances.
    %
    %
    % Methods:
    %   +   Init        Constructor method to properly initialize the class
    %   +   GetCal      Grabs the pixel calibration from a SIF file. Does not get called independently but inside AndorSIF.Read()
    %   +   Read:       Reads the data from a SIF file.
    %   +   Write       Write the imported SIF spectral data to a file for use with MATLab of LabViewRunTime programs.
    %   +   FrameID     Fast display of multiple frames, to identify where in the trigger scheme we started aquisition.
    %   +   DefineROI   Displays a plot of a frame from the SIF data to allow for convenient Region of Interest definition based on this actual measurement spectrum.
    %   +   Exitcode    Checks the returncodes of Andor SIF API function against possible returncodes and provides error handling/messages.
    %
    %
    %
    % To Do:
    %   +   Populate properties
    %   +   Create Constuctor method, which initialises the class.
    %   +   Create a method to get wavelength calibration from file, using atsif_getpixelcalibration or atsif_getpropertyvalue?
    %                            - Implement directly in AndorSIF.Read() so we only open/close the SIF once.
    %                            - Put calibrated wavelengths in a AndorSIF.property, so it accessable from/know in the instance.
    %   +   Exitcode method:
    %                            - take Andor SIF API numeric return codes, give the approriate returnmessage en raise an Exception when there has been an error.
    %                            - take an additional input(string) for additional details during troubleshooting, usefull for errors encoutered in the midst of reading a large series of images.
    %
    %   +   DefineROI method:
    %                             ! Use physical properties of the spectrum to determine the ROI('s). Perhaps implement in a seperate class.
    %                             - (use imrect to draw a rectangle and from this define a ROI.)
    %                             - Allow passing of extra argument to define multiple ROI's, but this should be done with different method which inherits from this one I guess.
    %                                 In fact, using multiple ROI's/reference beam should NOT be implemented in the class but from a different script that references/calls this class to work. Keep function in class simple/general!
    %
    %
    %   +   FrameID method:
    %                            -  Method to find the exposure order from the triggering scheme and to enable us to select the appropriate image for ROI definition.
    %                            -  Since each frame can be rather large (1M pixels) contourf() is slow since it tries to find contours. Rather use imagesc() as a faster alternative with smoothing for quick identification.

    properties
      succes = 0;         % Get sets to 1 if SIF has been read succesfully
      M = struct([])      % Struct contraining the data arrays for all the images in a SIF file and can be populated with fields for data only in ROI's.
      calibration = []    % Array where individual pixel wavelength calibration will be written to by AndorSIF.GetCal()
      Reference = []      % The frame number for AndorSIF.FrameID() for which both LED and Plasma are turned on, so we correctly can determine the absorption.
      frame_offset = 0    % Integer, invoke to intoduce an offset in the frame number we want to view, for example in obj.FrameID()
      ROI = []            % Array containing the vertices of the ROI for this sequence.
      I_PL = struct([])
      I_P = struct([])
      I_L = struct([])
      I_BG = struct([])
      test = []           % test property for debugging purpose. Usefull if we want extract a variable from the instance

    end

    methods
        function GetCal(obj)
          % Method that determines the amount of pixels in X direction and iterates over the pixels to get their calibrated wavelength according to the spectrograph.
          % Could be made more flexible using the Andor SIF API to first identify which axis corresponds to 'Wavelengt' using the aforementioned API, see atsif_getpropertyvalue().

          [rc,xsize] = atsif_getpropertyvalue(0,'DetectorFormatX');
          obj.Exitcode(rc);
          xsize =str2double(xsize); % convert from string to number.
          for i=1:xsize
            [rc,Lambda_pixel]=atsif_getpixelcalibration(0,0,i);
            % obj.Exitcode(rc) % checking the exitcode for each iteration leads to a significat decrease in performance, while it is not that important here since everything else went okay, this should too.
            calib(i)=Lambda_pixel;
          end
          obj.calibration=calib.';
        end

        function Read(obj,fpath,fname)
          % Method that calls the different MATLab implementations of the Andor SIF API (from dependencie /astifio) to read them into the struct M, which has been preallocated in properties.
          %
          % Arguments:
          %           +   fpath:    The path the to SIF file to be read
          %           +   fname:    The name of the SIF file to be read, including file extension.
          %
          % Outputs:
          %           None, though if there have been no errors encountered by the API during reading obj.succes is set to '1'.

            tic
            rc = atsif_readfromfile([fpath,fname]);
            obj.Exitcode(rc);
            if (rc == 22002) % if at any time an error is generated from the API functions (and thus rc!=22002) pipe this to exitcode function.
                signal=0; % Tells API to select the 'Signal' source channel of the SIF file. Perhaps move this to properties and let the value be selected at Initialisation, would mean also other channels could be selected this way..
                [rc,present]=atsif_isdatasourcepresent(signal);
                obj.Exitcode(rc);
                if present
                  [rc,ntot]=atsif_getnumberframes(signal);
                  obj.Exitcode(rc);
                  if (ntot>0)
                    [rc,size]=atsif_getframesize(signal);
                    obj.Exitcode(rc);
                    [rc,left,bottom,right,top,hBin,vBin]=atsif_getsubimageinfo(signal,0);%% function takes an index of which subimage to analyze as second argument.
                    obj.Exitcode(rc);
                    for i=1:ntot
                      [rc,data]=atsif_getframe(signal,i-1,size);
                      obj.Exitcode(rc); % perhaps Exitcode() has to be designed in such a way that it can accept additional input to display in the error message to enable easier troubleshooting if we encounter an error mid analysis.
                      obj.M(1,i).d=reshape(data,right,top); % Should M not be output, or updated in properties? M is 1 * ntot struct, with data shaped in 2D array by reshape, measuring right by top.
                    end
                  end
                end
            end
            obj.GetCal(); % Before closing the SIF file, read its pixel calibration
            atsif_closefile;
            obj.succes = 1; % set the property 'succes'  to 1 since SIF file has been read succesfully.
            sprintf('Succesfully read file from disk.')
            toc
        end

        function DefineROI(obj)
          % Method to define a ROI in the plotted spectrum.
          %
          % Arguments:
          %         + obj.M         The struct containing all SIF file images. From this a single frame must be selected for ROI definition in this method.
          %         + frame_number  the index of the frame from obj.M that we want to use to define a ROI.
          %
          % Outputs:
          %         + obj          self reference of the class object for the instance.
          %         + ROI_?        Coordinates of the rectangular ROI vertices.

          % disp('IMPORTANT: Rewrite for actual ROI based on physical characteristics of the spectra! DO NOT use a simple drawn rectangle to determine it. But we still could use it this way to limit our data set and split between reference and probe beam.');
          frame_number = obj.Reference;
          [dim.x, dim.y] = size(obj.M(frame_number).d); % get the size of image to set correct ticks on x axis.
          tick.x.increment = dim.x/4;
          tick.x.offset = dim.x/16; % value to offset xticks by so we do not end up with ticks on the ends of the plot
          figure('units','normalized','outerposition',[0 0 1 1])
          colormap('viridis');
          imagesc(obj.M(frame_number).d')
          set(gca,'YDir','normal');
          title('Drag Rectangle to define ROI, double click to accept.')
          xticks([(tick.x.increment-tick.x.offset) (2*tick.x.increment-tick.x.offset) (3*tick.x.increment-tick.x.offset) (4*tick.x.increment-tick.x.offset)]);
          xticklabels([obj.calibration(tick.x.increment-tick.x.offset) obj.calibration(2*tick.x.increment-tick.x.offset) obj.calibration(3*tick.x.increment-tick.x.offset) obj.calibration(4*tick.x.increment-tick.x.offset)])
          handleRect=imrect(gca,[0 0 2048,128]);
          obj.ROI=[obj.ROI;round(wait(handleRect),0)];% Appends the new coordinates of a new ROI to the already found ROI coordinates as a new row in a matrix. Coordinate format:  xmin,ymin,xsize,ysize
          close;

        end

        function AverageROI(obj)
          % Checks obj.ROI for the amount of rows, hence defined ROI's, and applies them on the frames, followed by averaging to get a averaged spectrum.
          %First apply the ROI's on the data and put this somewhere, so we can review them.
          numROI = size(obj.ROI(:,4));
          [~,numframes] = size(obj.I_PL);
          for i = 1:numROI(1)  % Nested for loops to apply the different ROI's to the data and write it to AndorSIF.M struct with variable field name depending on which ROI.
            dimROI = obj.ROI(i,:);
            yminROI = round(dimROI(2),0);
            yheightROI=round(dimROI(4),0);
            if yminROI == 0
              yminROI=yminROI+1;
            elseif (yminROI+yheightROI)>512
              yheightROI=512-yminROI;
            end
            fieldname=strcat('ROI',num2str(i));  % Set the fieldname for each specific ROI that is evaluated, so we can distinguish them in AndorSIF.M struct.
            fieldname_frameaverage=strcat(fieldname,'_frameaverage'); % separate fieldname for the averages over ROI per frame in AndorsIF.M.
            fieldname_Average=strcat(fieldname,'_Average'); % separate fieldname for the average spectrum over ROI and frames.
            % Declare the matrices with the average per frames for each phase per ROI
            obj.I_PL(1).(fieldname_Average)=[];
            obj.I_L(1).(fieldname_Average)=[];
            obj.I_P(1).(fieldname_Average)=[];
            obj.I_BG(1).(fieldname_Average)=[];

            for j = 1:numframes
              % lamp and Plasma
              obj.I_PL(j).(fieldname)(:,:)=obj.I_PL(j).d(:,yminROI:(yminROI+yheightROI));
              obj.I_PL(j).(fieldname_frameaverage)=mean(obj.I_PL(j).(fieldname),2); % Average for a single frame over ROI

              % lamp
              obj.I_L(j).(fieldname)(:,:)=obj.I_L(j).d(:,yminROI:(yminROI+yheightROI));
              obj.I_L(j).(fieldname_frameaverage)=mean(obj.I_L(j).(fieldname),2); % Average for a single frame over ROI

              % Plasma
              obj.I_P(j).(fieldname)(:,:)=obj.I_P(j).d(:,yminROI:(yminROI+yheightROI));
              obj.I_P(j).(fieldname_frameaverage)=mean(obj.I_P(j).(fieldname),2); % Average for a single frame over ROI

              % Background
              obj.I_BG(j).(fieldname)(:,:)=obj.I_BG(j).d(:,yminROI:(yminROI+yheightROI));
              obj.I_BG(j).(fieldname_frameaverage)=mean(obj.I_BG(j).(fieldname),2); % Average for a single frame over ROI

            end
            obj.I_PL(1).(fieldname_Average)=mean([obj.I_PL.(fieldname_frameaverage)],2);
            obj.I_L(1).(fieldname_Average)=mean([obj.I_L.(fieldname_frameaverage)],2);
            obj.I_P(1).(fieldname_Average)=mean([obj.I_P.(fieldname_frameaverage)],2);
            obj.I_BG(1).(fieldname_Average)=mean([obj.I_BG.(fieldname_frameaverage)],2);

          end

        end

        function FrameID(obj)
          % Method for fast viewing of multiple (consecutive) frames from a SIF file simultaneously. Allows for quick Identification where in the trigger scheme we started.
          %
          % Arguments
          %tic
          start = obj.frame_offset+1;  % Doing this we only have to change start to intoduce an offset to the four consecutive images we want to show.
          figure
          colormap('viridis')
          for i=1:4
            subplot(2,2,i);  % for now fixed to a 2*2 plot grid. Could make this dependend on a class property so we can vary with each instance how many plots we want to show.
            imagesc(obj.M(start+i-1).d'); % Use imagesc since it is much faster than contourf, the latter needs to figure out countours first.
            title(['Frame ' num2str(start+i-1)]);
            ylabel('Y position index');
            xlabel('Wavelength index');
            set(gca,'YDir','normal'); % Needed to set Y direction to normal
          end
          %toc
          sprintf('What frame corresponds to Plasma and LED on (answer with a number)?\nMake sure one of each is present:\n\n\t-Plasma+LED\n\t-LED only\n\t-Plasma only\n\t-Background.\n\nIf not, leave empty! Type a non numeric character to abort.')
          obj.Reference = input('','s');
          if isempty(obj.Reference)
            % if empty imput is passed, not all different images were visible and we have to iterate with an increased frame_offset.
            obj.frame_offset=obj.frame_offset+1;  % increase obj.frame_offset by one so we can try to find the four different pictures.
            close % prevent a plethora of figure windows to be created, especially important for multiple iterations.
            obj.FrameID(); % iterate over itself so we keep looking for a full set of the four different images.
          else
            obj.Reference=str2double(obj.Reference); % convert user input to a double
            if obj.Reference>0
              sprintf('Frame identified. Plasma and LED are both on in frame: %f', obj.Reference);
              close
            else
              close
              error('Detected abortion command, aborting at user request.')
            end

          end

          % Now, based on reference frame number, fill four vectors with the frame numbers corresponding to an exposure phase (e.g. PL,L,P,BG) and assign these vectors to the right quantities.
          [~,frames]=size(obj.M);
          FramesVec1 = [];    % First frame vector containing the frame numbers for the first exposure phase (either Pl,P,L,BG)
          FramesVec2 = [];    % Second frame vector for an exposure phase
          FramesVec3 = [];    % Third frame vector for an exposure phase
          FramesVec4 = [];    % Fourth frame vector for an exposure phase
          for i=1:4:frames
            FramesVec1= [FramesVec1,i];
            FramesVec2=[FramesVec2,i+1];
            FramesVec3 = [FramesVec3,i+2];
            FramesVec4 = [FramesVec4,i+3];
          end
          if (mod(obj.Reference,2)==0 && mod(obj.Reference,4)==0) % WARNING: This only works for a Reference below the 8th frame! If the first few frames are garbage, they need to be removed manually.
            % set stuff so we apply correct vectors to correct phases. For the source data, use the found ROI's
            frameI_PL=FramesVec4;
            frameI_L=FramesVec1;
            frameI_P=FramesVec2;
            frameI_BG=FramesVec3;
          elseif (mod(obj.Reference,2)==0 && mod(obj.Reference,4)~=0)
            frameI_PL=FramesVec2;
            frameI_L=FramesVec3;
            frameI_P=FramesVec4;
            frameI_BG=FramesVec1;
          elseif mod(obj.Reference,3)==0
            frameI_PL=FramesVec3;
            frameI_L=FramesVec4;
            frameI_P=FramesVec1;
            frameI_BG=FramesVec2;
          else
            frameI_PL=FramesVec1;
            frameI_L=FramesVec2;
            frameI_P=FramesVec3;
            frameI_BG=FramesVec4;
          end

          for i=1:length(frameI_PL)
            obj.I_PL(i).d = obj.M(frameI_PL(i)).d;
            obj.I_L(i).d = obj.M(frameI_L(i)).d;
            obj.I_P(i).d = obj.M(frameI_P(i)).d;
            obj.I_BG(i).d = obj.M(frameI_BG(i)).d;
          end
        end

        function Exitcode(obj,ASIFreturncode)
          % Method to check Andor SIF API exitcodes against possible exit codes. Raises an error and displays error information when an error has been encountered by the API.
            Returnmsg=atsif_exitcode(ASIFreturncode);
            if (ASIFreturncode==22002)
              return
            else %% add an if statement to check for additional input argument. If it is default/empty as below, otherwise also print additonal error info.
              error('WARNING: Error encountered!\n\nRaise Exception: %s',Returnmsg)
            end
        end


        function Write(obj,sourcepath,sourcename,AbsorbProbeRef, AbsorbProbe)
          % Declare some initial stuff for before writing.
          formatSpec = '%f\t%f\n'; % formatting of the data to be written.
          ext = '.txt';
          writepath=sourcepath;
          writename=split(sourcename,'.');
          writename=writename(1);

          % Set desired variables we want to save
          disp('Manually declare the variables of the class we want to save. Automatically adjust for amount of ROI.')
          [numROI,lenROI]=size(obj.ROI);

          % Generate a struct containing all the averages over ROI and frames for each phase in separate fields.
          % Field names are generated dynamically based on the amount of ROI's
          for i=1:numROI
              varIPL = matlab.lang.makeValidName(strcat('AV.I_PL_ROI',num2str(i)));
              varIPL=strrep(varIPL,'AV_','AV.'); %matlab.lang.makeValidName replaces the dot with an underscore, which is NOT what we need! We want all of these variables to be written to the AV struct!
              eval([varIPL,'= obj.I_PL.ROI',num2str(i),'_Average;']);
              varIL = matlab.lang.makeValidName(strcat('AV.I_L_ROI',num2str(i)));
              varIL=strrep(varIL,'AV_','AV.');
              eval([varIL, '= obj.I_L.ROI',num2str(i),'_Average;']);
              varIP = matlab.lang.makeValidName(strcat('AV.I_P_ROI',num2str(i)));
              varIP=strrep(varIP,'AV_','AV.');
              eval([varIP, '= obj.I_P.ROI',num2str(i),'_Average;']);
              varBG = matlab.lang.makeValidName(strcat('AV.I_BG_ROI',num2str(i)));
              varBG=strrep(varBG,'AV_','AV.');
              eval([varBG, '= obj.I_BG.ROI',num2str(i),'_Average;']);

          end
          absolutepath=strcat(writepath,writename,'.mat');
          calib=obj.calibration;

          %Check if the additional parameters to be saved are present in argument.
          switch nargin
            case 3
              InArg=0;
              fprintf('Saving desired workspace variables...\nAbsolute Path:\n\t%s\n', absolutepath)

            case 4
              AV.AbsorbRef=AbsorbProbeRef;
              WriteArray=[AV.AbsorbRef];
              InArg=1;
              writename=strcat(writename,'_abs',ext);
              fprintf('Saving desired workspace variables and Absorption with Reference...\nAbsolute Path:\n\t%s\n\nAdditional File:\n\t%s\n', [absolutepath,writename(1)])

            case 5
              AV.AbsorbProbeRef=AbsorbProbeRef;
              AV.AbsorbProbe=AbsorbProbe;
              InArg=2;
              WriteArray(:,1)=AV.AbsorbProbeRef;
              WriteArray(:,2)=AV.AbsorbProbe;
              writename=[strcat(writename,'_abs',ext); strcat(writename,'_probe',ext)];
              fprintf('Saving desired workspace variables and Absorption with and without Reference...\nAbsolute Path:\n\t%s\n\nAdditional Files:\n\t%s\n\t%s\n', [absolutepath,writename(1,:),writename(2,:)])

          end
          % save the desired workspace variables of an instance of AndorSIF
          save(char(absolutepath), 'calib' ,'AV');
          obj.test=WriteArray;

          % save the additional supplied parameters in seperate .txt files in the format used by the LabViewRunTime program.
          if InArg>0
            for i=1:InArg
              f.ID=fopen(strcat(writepath,writename(i)),'w');
              for j=1:length(calib)
                fprintf(f.ID,formatSpec,calib(j),WriteArray(j,i));
              end
              fclose(f.ID);
            end
          end

        end



    end

end
