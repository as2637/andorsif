[f.name,f.path]=uigetfile('*.sif');
s=AndorSIF();
s.Read(f.path,f.name);
s.FrameID();
s.DefineROI();
s.DefineROI();
s.AverageROI();

AbsorpROI2 = -log((s.I_PL(1).ROI2_Average-s.I_P(1).ROI2_Average)./(s.I_L(1).ROI2_Average-s.I_BG(1).ROI2_Average));
AbsorpRef = -log(((s.I_PL(1).ROI2_Average-s.I_P(1).ROI2_Average)./(s.I_PL(1).ROI1_Average-s.I_P(1).ROI1_Average))./((s.I_L(1).ROI2_Average-s.I_BG(1).ROI2_Average)./(s.I_L(1).ROI1_Average-s.I_BG(1).ROI1_Average)));

s.Write(f.path,f.name,AbsorpRef,AbsorpROI2);
