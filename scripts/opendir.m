fprintf('Starting batch import of *.sif files\n')
direct.path=uigetdir('Open folder containing *.sif files');
if direct.path==0
    error('No directory path was passed. No files will be opened. Stopped running script.')
end
direct.path=strcat(direct.path,'\'); % add the trailing backslash so we use can use full file paths
direct.files=dir(strcat(direct.path,'\*.sif'));

% iterate over the files in the folder
file_amount=length(direct(1).files);
for i=1:file_amount
  fprintf('Processing file %.0f of %.0f\n',[i,file_amount]);
  s=AndorSIF();
  s.Read(direct.path,direct(1).files(i).name);
  s.FrameID();

  % Since ROI's are pretty consistent (which is an essential requirement and the reason for a stable LED) only determine a ROI once and use the same one for all frames.
  switch i
    case 1
      s.DefineROI();
      s.DefineROI();
      GeneralROI=s.ROI;
    otherwise
      s.ROI=GeneralROI; % Applies the ROI's defined for i=1 to each consecutive frame by setting the instance property s.ROI.
  end

    s.AverageROI();

    % calculate absorbances
    AbsorpROI2 = -log((s.I_PL(1).ROI2_Average-s.I_P(1).ROI2_Average)./(s.I_L(1).ROI2_Average-s.I_BG(1).ROI2_Average));
    AbsorpRef = -log(((s.I_PL(1).ROI2_Average-s.I_P(1).ROI2_Average)./(s.I_PL(1).ROI1_Average-s.I_P(1).ROI1_Average))./((s.I_L(1).ROI2_Average-s.I_BG(1).ROI2_Average)./(s.I_L(1).ROI1_Average-s.I_BG(1).ROI1_Average)));

    % save results
    s.Write(direct.path,direct(1).files(i).name,AbsorpRef,AbsorpROI2)
end

fprintf('Batch import completed!\n\tImported files: %.0f\n', i);
