function returnmessage=atsinf_exitcode(rc)
  % Function that translates numeric return/exit codes from Andor SIF API into their respective error string as documented in the API docs using a MATLab map object.
  %
  % Arguments:  rc          The numeric API return code, e.g. 22002, which means 'ATSIF_SUCCES'
  % Return:     returncode  The same numeric API return code as the input argument
  %             Returnmessage  The string corresponding to the numeric returncode, e.g. 'ATSIF_SUCCES' for '22002'
  %
  %Notes: Possible string-code pairs are:
  %
  %       ATSIF_SUCCES                22002
  %
  %       ATSIF_SIF_FORMAT_ERROR      22003
  %       ATSIF_NO_SIF_LOADED         22004
  %       ATSIF_FILE_NOT_FOUND        22005
  %       ATSIF_FILE_ACCES_ERROR      22006
  %       ATSIF_DATA_NOT_PRESENT      22007
  %
  %       ATSIF_P1INVALID             22101
  %       ATSIF_P2INVALID             22102
  %       ATSIF_P3INVALID             22103
  %       ATSIF_P4INVALID             22104
  %       ATSIF_P5INVALID             22105
  %       ATSIF_P6INVALID             22106
  %       ATSIF_P7INVALID             22107
  %       ATSIF_P8INVALID             22108

  keyReturnMessage = {'ATSIF_SUCCES', 'ATSIF_SIF_FORMAT_ERROR','ATSIF_NO_SIF_LOADED','ATSIF_FILE_NOT_FOUND','ATSIF_FILE_ACCES_ERROR','ATSIF_DATA_NOT_PRESENT', 'ATSIF_P1INVALID','ATSIF_P2INVALID', 'ATSIF_P3INVALID','ATSIF_P4INVALID','ATSIF_P5INVALID','ATSIF_P6INVALID','ATSIF_P7INVALID','ATSIF_P8INVALID'};
  valueReturnMessage = [22002,22003,22004,22005,22006,22007,22101,22102,22103,22104,22105,22106,22107,22108];
  mapObj = containers.Map(valueReturnMessage,keyReturnMessage);
  returnmessage = mapObj(rc);
