# AndorSIF README #
***
## What is this repository for? ##

### Quick summary ###
Andor Solis spectrographs store their files in a proprietary format (_\*.sif_ extension), which can contain long sequences of uncompressed images, which can be rather large files (sizes > 1GB are far from extraordinary). Instead of using these files with their own software, Andor has an API to import these files in other programs such as MATLAB. On Windows this is provided with a _\*.dll_ library (different one for _x64_ systems) and _*.mex_ (MATLAB executable) files. These library files, the API documentation and MATLAB code that calls these API functions are all contained within the folder `~/atsifio`.

Using these resources, the aim of this repository is to create a new, object oriented, MATLAB implementation of the API, that provides all necessary operations a user could want to perform, by only invoking a (couple of) simple commands. The goal is that a user can concern himself/herself with the actual data, instead of continuously having to retype commands with minimal variation.

At the moment, the AndorSIF class has been written mainly with the two beam absorption setup at YPI labs in mind, however by using class inheritance it is rather easy to adjust/adapt the code for different experiments. If there is demand for it, I might split the class in a more general base class and experiment specific subclasses.
***
## Installation ##
Simply clone/download the repository and make sure you add it to the MATLAB path including subdirectories. Included with the installation are:

*   The AndorSIF API _\*.dll_ files for Windows (and the MATLAB functions implementing them) are included in `~/atsifio`.
*   Default colourmap for images is _'viridis'_  (found in `~/lib`) which is the default colourmap of SciPy, because of it's superior readability (see for more info: [this github page](https://bids.github.io/colormap/)).
*   Some hopefully useful scripts (in `~/scripts`) to use or serve as examples.

### Dependencies ###
Make sure you have each of the following, to ensure that everything runs instead of producing errors.

*   `Windows 7+`; lower versions not supported by MATLAB2016b+, Linux not tested.
*   `MATLAB 2016b+`; for earlier releases you'd have to adjust some functions yourself.
*   `MATLAB Image Processing Toolbox`; User scalable/moveable box to define ROI's depends on this.

### Steps to get set up ###

*   Download/clone repository to a folder of your choosing (called Folder for now)
*   If downloading: make sure the files are extracted from the archive
*   Launch MATLAB and add Folder to the MATLAB path (including subfolders)
*   You can now call _AndorSIF_ in your own scripts, or use the bundled scripts.
***
## Examples ##
##### Read a file, apply two ROI's and determine absorbance from them #####
Below is an example in which the user supplies a file via a dialog, which is then read.
After identification of the desired frame, two different ROI's are defined by the user and we determine the average spectrum over all frames for each of them. Using these results we calculate the Absorbance of ROI2  and this Absorbance corrected by ROI1. We write our results to a `*.mat` file and additional `*.txt` files containing these two seperate absorbances.
```
#!matlab
[f.name,f.path]=uigetfile('*.sif');
s=AndorSIF();
s.Read(f.path,f.name);
s.FrameID();
s.DefineROI();
s.DefineROI();
s.AverageROI();

AbsorpROI2 = -log((s.I_PL(1).ROI2_Average-s.I_P(1).ROI2_Average)./(s.I_L(1).ROI2_Average-s.I_BG(1).ROI2_Average));
AbsorpRef = -log(((s.I_PL(1).ROI2_Average-s.I_P(1).ROI2_Average)./(s.I_PL(1).ROI1_Average-s.I_P(1).ROI1_Average))./((s.I_L(1).ROI2_Average-s.I_BG(1).ROI2_Average)./(s.I_L(1).ROI1_Average-s.I_BG(1).ROI1_Average)));

s.Write(f.path,f.name,AbsorpRef,AbsorpROI2);
```
Calculations:
$$A_{Probe}=-log\left(\frac{I_{PL}-I_{P}}{I_{L}-I_{BG}}\right)$$
$$A_{ProbeRef}=-log\left(\frac{\frac{I_{PL,probe}-I_{P,probe}}{I_{PL,ref}-I_{P,ref}}}{\frac{I_{L,probe}-I_{BG,probe}}{I_{L,ref}-I_{BG,ref}}} \right)$$
##### Open a directory of files, Identify two ROI's and apply them to all files #####
In fact we do the same as the above example but now for a whole series of files. We only identify our two desired ROI's once, then apply those same ROI's for all files, by setting the coordinates/dimensions in the instance property `s.ROI`.
```
#!matlab
fprintf('Starting batch import of *.sif files\n')
direct.path=uigetdir('Open folder containing *.sif files');
if direct.path==0
    error('No directory path was passed. No files will be opened. Stopped running script.')
end
direct.path=strcat(direct.path,'\'); % add the trailing backslash so we use can use full file paths
direct.files=dir(strcat(direct.path,'\*.sif'));

% iterate over the files in the folder
file_amount=length(direct(1).files);
for i=1:file_amount
  fprintf('Processing file %.0f of %.0f\n',[i,file_amount]);
  s=AndorSIF();
  s.Read(direct.path,direct(1).files(i).name);
  s.FrameID();

  % Since ROI's are pretty consistent only determine a ROI once and use the same one for all frames.
  switch i
    case 1
      s.DefineROI();
      s.DefineROI();
      GeneralROI=s.ROI;
    otherwise
      s.ROI=GeneralROI; % Applies the ROI's defined for i=1 to each consecutive frame by setting the instance property s.ROI.
  end

    s.AverageROI();

    % calculate absorbances
    AbsorpROI2 = -log((s.I_PL(1).ROI2_Average-s.I_P(1).ROI2_Average)./(s.I_L(1).ROI2_Average-s.I_BG(1).ROI2_Average));
    AbsorpRef = -log(((s.I_PL(1).ROI2_Average-s.I_P(1).ROI2_Average)./(s.I_PL(1).ROI1_Average-s.I_P(1).ROI1_Average))./((s.I_L(1).ROI2_Average-s.I_BG(1).ROI2_Average)./(s.I_L(1).ROI1_Average-s.I_BG(1).ROI1_Average)));

    % save results
    s.Write(direct.path,direct(1).files(i).name,AbsorpRef,AbsorpROI2)
end

fprintf('Batch import completed!\n\tImported files: %.0f\n', i);

```